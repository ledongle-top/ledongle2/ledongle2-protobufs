#!/bin/bash

protoheader=$1/c_out/effects/protos.h
effectselector=$1/c_out/effects/effect_selector.cblock
all_effects_header=$1/c_out/effects/effects.h
effectstateselector=$1/c_out/effects/effect_state_selector.cblock


echo "#ifndef PROTOS_H
#define PROTOS_H" > $protoheader


array="static const char* effect_protos[]={"

echo "" > $effectselector
echo "" > $all_effects_header
echo "" > $effectstateselector



for f in $1/effects/*.proto; do
    IFS= read -r -d '' content <$f
    content=${content//'"'/'\"'/}
    content=${content//$'\n'/'\n'}
    name=${f%%.proto}
    name=${name##*/}
    echo "static const char ${name}_proto[] = \"$content\";" >> $protoheader
    array+=" ${name}_proto,"

    echo "case EFFECT_ARGUMENTS__EFFECT_ARGUMENTS_${name^^}_ARGUMENTS:" >> $effectselector
    echo "return ${name};" >> $effectselector

    echo "case EFFECT_ARGUMENTS__EFFECT_ARGUMENTS_${name^^}_ARGUMENTS:" >> $effectstateselector
    echo  "res = malloc(sizeof(${name^}State));" >> $effectstateselector
    echo "${name}_state__init((${name^}State*)res);" >> $effectstateselector
    echo "config -> effect_state -> ${name}state = (${name^}State*)res;" >> $effectstateselector
    echo "break;" >> $effectstateselector

    effectheader="$1/c_out/${name}.h"
    effectcpp="$1/c_out/${name}.cpp"
    echo "#include \"WS2812_strip.h\"" > ${effectheader}
    echo "#include \"strip_config.pb-c.h\"" >> ${effectheader}
    echo "int ${name} (WS2812_strip* strip, EffectArguments *effect_args, EffectState *effect_state);" >> ${effectheader}

    echo "#include \"WS2812_strip.h\"" > ${effectcpp}
    echo "#include \"color.h\"" >> ${effectcpp}
    echo "#include \"WS2812_effects.h\"" >> ${effectcpp}
    echo "#include \"${name}.h\""  >> ${effectcpp}
    echo "int ${name} (WS2812_strip* strip, EffectArguments *effect_args, EffectState *effect_state){" >> ${effectcpp}
    echo "${name^}Arguments *args = effect_args -> ${name}arguments;" >> ${effectcpp}
    echo "${name^}State *state = effect_state -> ${name}state;">> ${effectcpp}
    echo "#include \"../effect_src/${name}.effect\"" >> ${effectcpp}
    echo "}" >> ${effectcpp}

    echo "#include \"${name}.h\"" >> $all_effects_header
done

array=${array/%,/ \}\;}
echo $array >> $protoheader
echo "#endif" >> $protoheader
