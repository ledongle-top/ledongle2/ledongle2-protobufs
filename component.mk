COMPONENT_EXTRA_CLEAN = c_out/*.h c_out/*.c c_out/effects/*.c c_out/effects/*.h c_out/*.cpp c_out/effects/*.cpp
COMPONENT_ADD_INCLUDEDIRS = c_out c_out/effects
COMPONENT_SRCDIRS = . c_out c_out/effects

$(COMPONENT_LIBRARY): protos.h

.PHONY: protos.h

protos.h:
	protoc --c_out=$(COMPONENT_PATH)/c_out -I$(COMPONENT_PATH) $(COMPONENT_PATH)/*.proto $(COMPONENT_PATH)/effects/*.proto && $(COMPONENT_PATH)/generate_header.sh $(COMPONENT_PATH)
