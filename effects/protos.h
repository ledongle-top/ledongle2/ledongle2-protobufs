#ifndef PROTOS_H
#define PROTOS_H
static const char rainbow_proto[] = "//Rainbow\nsyntax=\"/proto3\"/;\n\nmessage RainbowArguments{\n  uint32 brightness = 1;\n  uint32 speed = 2;\n}\n\nmessage RainbowState{\n  uint32 hue;\n}\n";
static const char solid_proto[] = "//Solid\nsyntax = \"/proto3\"/;\n\nmessage SolidArguments{\n  uint32 color = 1;\n}\n\nmessage SolidState{\n  float fill_progress = 1;\n}";
static const char* effect_protos[]={ rainbow_proto, solid_proto };
#endif
